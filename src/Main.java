import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();
        BigDecimal bd = BigDecimal.valueOf(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public static List<CalcList> calc(double credit, double percentage, int month) {
        ArrayList<CalcList> list = new ArrayList<>();
        double i = percentage / 1200;
        double totalPayment = round(credit * ((i * Math.pow(1 + i, month)) / (Math.pow(1 + i, month) - 1)), 2);
        for (int j = 1; j <= month; j++) {
            CalcList calcList = new CalcList();
            double percentageForList;
            if (j == 1) {
                percentageForList = round(credit * i, 2);
                calcList.setCreditBalance(credit);
            } else {
                calcList.setCreditBalance(round(list.get(j - 2).getCreditBalance() - list.get(j - 2).getMainDebt(), 2));
                percentageForList = round(calcList.getCreditBalance() * i, 2);
            }
            double mainDebtForList = round(totalPayment - percentageForList, 2);
            calcList.setMonth(j);
            calcList.setMainDebt(mainDebtForList);
            calcList.setPercentage(percentageForList);
            calcList.setTotalPayment(totalPayment);
            list.add(calcList);
        }
        return list;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter creditAmount, percentage and month: ");
        List<CalcList> calc = calc(scanner.nextDouble(), scanner.nextDouble(), scanner.nextInt());
        calc.forEach(System.out::println);
        double sum = calc.stream().filter(o -> o.getMainDebt() > 0).mapToDouble(CalcList::getMainDebt).sum();
        double percentage = calc.stream().filter(o -> o.getPercentage() > 0).mapToDouble(CalcList::getPercentage).sum();
        double totalPayment = calc.stream().filter(o -> o.getTotalPayment() > 0).mapToDouble(CalcList::getTotalPayment).sum();
        System.out.println(Math.round(sum));
        System.out.println(round(percentage,2));
        System.out.println(round(totalPayment, 2));
    }
}

class CalcList {
    public int month;
    public double creditBalance;
    public double mainDebt;
    public double percentage;
    public double totalPayment;

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public double getCreditBalance() {
        return creditBalance;
    }

    public void setCreditBalance(double creditBalance) {
        this.creditBalance = creditBalance;
    }

    public double getMainDebt() {
        return mainDebt;
    }

    public void setMainDebt(double mainDebt) {
        this.mainDebt = mainDebt;
    }

    public double getPercentage() {
        return percentage;
    }

    public void setPercentage(double percentage) {
        this.percentage = percentage;
    }

    public double getTotalPayment() {
        return totalPayment;
    }

    public void setTotalPayment(double totalPayment) {
        this.totalPayment = totalPayment;
    }

    @Override
    public String toString() {
        return "CalcList{" +
                "month=" + month +
                ", creditBalance=" + creditBalance +
                ", mainDebt=" + mainDebt +
                ", percentage=" + percentage +
                ", totalPayment=" + totalPayment +
                '}';
    }
}